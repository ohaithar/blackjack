<?php
session_start();
$flag = 'CWN{automate_the_boring_stuff}';
$suits = array ("spades", "hearts", "clubs", "diamonds");
$faces = array (
    "2"=>2, "3"=>3, "4"=>4, "5"=>5, "6"=>6, "7"=>7, "8"=>8,
    "9"=>9, "10"=>10, "jack"=>10, "queen"=>10, "king"=>10, "ace"=>11
);

function evaluateHand($hand) {
    global $faces;
    $value = 0;
    foreach ($hand as $card) {
        if ($value > 11 && $card['face'] == 'ace') {
            $value = $value + 1;
        } else {
            $value = intval($value) + intval($faces[$card['face']]);
        }
    }
    return $value;
}

function build_deck() {
	global $suits;
	global $faces;
	$deck = array();
	foreach ($suits as $suit) {
	    $keys = array_keys($faces);
	    foreach ($keys as $face) {
	        $deck[] = array('face'=>$face, 'suit'=>$suit);
	    }
	}
	shuffle($deck);
	return $deck;
}

function build_hands($deck) {
	$hand = array();
	for ($i = 0; $i < 2; $i++) {
        $hand[] = array_shift($deck);
        $dealer[] = array_shift($deck);
    }
    return array("hand"=>$hand,"dealer"=>$dealer,"deck"=>$deck);
}

function stow_vals($vals) {
	foreach ($vals as $key=>$val) {
		$_SESSION[$key] = $val;
	}
}

if (isset($_SESSION['hai'])) {
	@extract($_REQUEST);
	if (isset($do_what)) {
		if ($_SESSION['wins'] > 2) {
			header('Content-Type: application/json');
			echo json_encode(array("flag"=>$flag));
			die();
		}
		if ($do_what == 'deal') {
			$_SESSION['game_over'] = false;
			$deck = build_deck();
			$hands = build_hands($deck);
			$hand_val = evaluateHand($hands["hand"]);
			$dealer_val = evaluateHand($hands["dealer"]);
			$msg = ($hand_val == 21 ? 'winner' : 'carry on');
			if ($dealer_val == 21) {
				$msg = 'DEALER 21';
				$_SESSION['game_over'] = true;
			} else if ($msg == 'winner') {
				$_SESSION['game_over'] = true;
				$_SESSION['wins'] += 1;
			}
			stow_vals(array('deck'=>$hands['deck'],'hand_val'=>$hand_val,'hand'=>$hands['hand'],'dealer'=>$hands['dealer'],'dealer_val'=>$dealer_val));
			header('Content-Type: application/json');
			if ($dealer_val != 21) {
				echo json_encode(array("hand_value"=>$_SESSION['hand_val'],"hand"=>$hands["hand"],"dealer"=>array($hands['dealer'][0]),"msg"=>$msg,"wins"=>$_SESSION['wins']));
			} else {
				echo json_encode(array("hand_value"=>$_SESSION['hand_val'],"hand"=>$hands["hand"],"dealer"=>$hands['dealer'],"msg"=>$msg));
			}
		} else if ($do_what == 'hit') {
			if ($_SESSION['game_over']) {
				header('Content-Type: application/json');
				echo json_encode(array("msg"=>"...tard"));
			}
			if ($_SESSION['hand_val'] < 21) {
				$hand_val = $_SESSION['hand_val'];
				$hand = $_SESSION['hand'];
				$deck = $_SESSION['deck'];
				$new_card = array_shift($deck);
				array_push($hand, $new_card);
				$hand_val = evaluateHand($hand);
				$msg = ($hand_val > 21 ? 'bust' : 'carry on');
				stow_vals(array('deck'=>$deck,'hand_val'=>$hand_val,'hand'=>$hand));
				header('Content-Type: application/json');
				echo json_encode(array("hand_value"=>$_SESSION['hand_val'],"hand"=>$hand,"msg"=>$msg));
			} else if ($_SESSION['hand_val'] == 21) {
				header('Content-Type: application/json');
				echo json_encode(array("msg"=>"...tard"));
			} else if ($_SESSION['hand_val'] > 21) {
				header('Content-Type: application/json');
				echo json_encode(array("msg"=>"...ya done, son"));
			}
		} else if ($do_what == 'stay') {
			if ($_SESSION['game_over']) {
				header('Content-Type: application/json');
				echo json_encode(array("msg"=>"Game Over..."));
			}
			$deck = $_SESSION['deck'];
			$hand_val = $_SESSION['hand_val'];
			$dealer_val = $_SESSION['dealer_val'];
			$dealer_hand = $_SESSION['dealer'];
			if ($dealer_val > $hand_val) {
				$_SESSION['game_over'] = true;
				header('Content-Type: application/json');
				echo json_encode(array("msg"=>"Dealer Win","dealer"=>$dealer_hand,"dealer_val"=>$dealer_val,"hand_val"=>$hand_val));
			} else {
				while ($dealer_val < 17) {
					$new_card = array_shift($deck);
					array_push($dealer_hand, $new_card);
					$dealer_val = evaluateHand($dealer_hand);
				}
				
				if (($dealer_val < 22) && ($dealer_val >= $hand_val)) {
					$msg = "Dealer Win";
				} else if (($dealer_val < $hand_val) || ($dealer_val > 21)) {
					$msg = "winner";
					$_SESSION['wins'] += 1;
				}
				$_SESSION['game_over'] = true;
				header('Content-Type: application/json');
				echo json_encode(array("msg"=>$msg,"dealer"=>$dealer_hand));
			}
		}
	} else {
		var_dump($_POST);
		die();
	}
} else {
	header('Content-Type: application/json');
	echo json_encode(array('sess_data' => 'noesbueno'));
}


